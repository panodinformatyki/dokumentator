from django.db import models

# Create your models here.
class Lists(models.Model):
    numer_spisu = models.CharField(max_length=10, null=True, blank=True)
    file_to_upload = models.FileField(upload_to='storage/', null=True, blank=True, verbose_name="Wzór spisu Z-O")
    szo_after_gen = models.FileField(blank=True, help_text='Plik ze spisem zdawczo-odbiorczym', verbose_name="Spis Z-O")

    # TODO: Stworzyć polskie nazwy w panelu adminsitratora. class meta verbose_name i verbose_plural_name
    def __str__(self):
        return self.numer_spisu

    class Meta:
        verbose_name = 'Spis Z-O'
        verbose_name_plural = "Spisy Z-O"

class RangeBox(models.Model):
    number_box = models.IntegerField()
    od = models.IntegerField()
    do = models.IntegerField()
    lists = models.ForeignKey(Lists, on_delete=models.CASCADE, null=True,)

    def __str__(self):
        return str(self.number_box)
