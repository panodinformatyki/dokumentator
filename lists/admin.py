from django.contrib import admin
from django.http import HttpResponse

from .models import Lists, RangeBox
from briefcases.models import BriefCase
from client.models import Klient
from .szo_generator import gen_xlsx_file_with_szo
from .sticker_lists_generator import sticker_lists_generator, sticker_box_generator

import pathlib

import zipfile



def generator_szo(modeladmin, request, queryset):

    #print(queryset[2])
    list_to_szo = []
    list_to_stickers_up = []
    list_to_stickers_down = []
    list_one_record = []
    for nazwa in queryset:
        lista_teczek = BriefCase.objects.filter(numer_spisu__numer_spisu=nazwa)
        lista_teczek = lista_teczek.order_by('jrwa__znak')
        name_client = lista_teczek[0].jrwa.klient.nazwa
        klient = Klient.objects.get(nazwa=name_client)
        conf_to_stickers = [klient.field_1, klient.field_2, klient.field_3, klient.field_4, klient.field_5]
        conf_to_stickers = [ elem for elem in conf_to_stickers if elem is not None]
        tytul_teczki = []
        lista_to_str_tmp = []
        tytul_teczki_to_str = []
        for one_conf in conf_to_stickers:
            dupa = lista_teczek.values_list(one_conf)
            tytul_teczki.append(list(dupa))

        for ii in range(0, len(tytul_teczki[0])):
            for jj in range(0, len(tytul_teczki)):
                lista_to_str_tmp.append(tytul_teczki[jj][ii][0])
            tytul_teczki_to_str.append(lista_to_str_tmp)
            lista_to_str_tmp = []

        path_file = pathlib.Path.cwd() / 'storage/klient' / name_client / str(nazwa)
        if not path_file.exists():
           path_file.mkdir(parents=True, exist_ok=True)
        else:
            print('file_exist')
        iii = 0
        str_to_tytul_teczki = '\n'
        for teczka in lista_teczek:
            tytul_teczki = str_to_tytul_teczki.join(tytul_teczki_to_str[iii])
            iii = iii+1
            for i in range(1, teczka.tom + 1):
                true_tytul_teczki = tytul_teczki
                znak_teczki = teczka.jrwa.klient.skrot_departamentu + '-' + teczka.jrwa.znak_jrwa
                if teczka.tom == 1:
                    tytul_teczki = tytul_teczki
                else:
                    tytul_teczki =  tytul_teczki + ' Tom. ' + str(i)
                daty_skrajne = str(teczka.data_rozpoczecia) + '-' + str(teczka.data_zakonczenia)
                kat_arch = teczka.kategoria_archiwalna
                liczba_teczek = '1'
                uwagi = teczka.uwagi
                data_zalozenia = teczka.data_rozpoczecia
                list_one_record = [znak_teczki, tytul_teczki, data_zalozenia, daty_skrajne, kat_arch, liczba_teczek, '', '', uwagi]
                list_to_szo.append(list_one_record)
                name_client = teczka.jrwa.klient.nazwa
                departament_client = teczka.jrwa.klient.departament
                list_one_record = [name_client, departament_client, znak_teczki + '                       ' + kat_arch]
                list_to_stickers_up.append(list_one_record)
                list_one_record = [tytul_teczki, "Daty skrajne: " + daty_skrajne]
                list_to_stickers_down.append(list_one_record)
                tytul_teczki = true_tytul_teczki
        for add_lp in range(0,len(list_to_szo)):
            add_do_lp = add_lp + 1
            list_to_szo[add_lp].insert(0, add_do_lp)
            list_to_stickers_down[add_lp].append(str(nazwa) + '/' + str(add_do_lp))
        filename = nazwa
        xlsx_path = filename.file_to_upload.path
        print(xlsx_path)
        print(list_to_szo[0])
        response, filename_with_szo = gen_xlsx_file_with_szo(xlsx_path, list_to_szo, path_file)
        sticker_lists_generator(list_to_stickers_up, list_to_stickers_down, path_file)
        filename.szo_after_gen.name = str(filename_with_szo)
        filename.save()
        list_to_stickers_down = []
        list_to_stickers_up = []
        list_one_record = []
        list_to_szo = []
    ## wyrzuci spis do pobrania ;-)
    # response = HttpResponse(list_to_szo, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    # response['Content-Disposition'] = 'attachment; filename="szo.xlsx"'

    return response


generator_szo.short_description = 'Generuj spis z-o'

def generator_zip(modeladmin, request, queryset):
    for one in queryset:
        response = HttpResponse(content_type='application/zip')
        lista_teczek = BriefCase.objects.filter(numer_spisu__numer_spisu=one)
        zip_file = zipfile.ZipFile(response, 'w')
        name_client = lista_teczek[0].jrwa.klient.nazwa
        path_file = pathlib.Path.cwd() / 'storage/klient' / name_client / str(one)
        for f in path_file.iterdir():
            zip_file.write(f, arcname=f.name)
        response['Content-Disposition'] = 'attachment; filename="{}.zip"'.format(path_file.name)
    return response

generator_zip.short_description = "Pobierz ZIP z SZO i naklejkami"

def generator_stickers_box(modeladmin, request, queryset):
    for one in queryset:
        lista_teczek = BriefCase.objects.filter(numer_spisu__numer_spisu=one)
        name_client = lista_teczek[0].jrwa.klient.nazwa
        path_file = pathlib.Path.cwd() / 'storage/klient' / name_client / str(one)
        range_box = RangeBox.objects.filter(lists__numer_spisu=one)
        to_stickers = []
        lista_to_append = [lista_teczek[0].jrwa.klient.nazwa, lista_teczek[0].jrwa.klient.departament,
                           lista_teczek[0].kategoria_archiwalna]
        number_spisu = 'Spis ' + str(one)
        lista_to_append.append(number_spisu)
        for one_carton in range_box:
            carton_number = 'KARTON ' + str(one_carton.number_box)
            lista_to_append.append(carton_number)
            carton_position = 'Poz. ' + str(one_carton.od) + '-' + str(one_carton.do)
            lista_to_append.append(carton_position)
            to_stickers.append(lista_to_append)
            lista_to_append = [lista_teczek[0].jrwa.klient.nazwa, lista_teczek[0].jrwa.klient.departament,
                               lista_teczek[0].kategoria_archiwalna, 'Spis ' + str(one)]
        print(to_stickers)
        sticker_box_generator(to_stickers, path_file)
    return request

generator_stickers_box.short_description = "Generuj naklejki na pudelka"

class RangeBoxInline(admin.TabularInline):
    model = RangeBox

class ListsAdmin(admin.ModelAdmin):
    actions = [generator_szo, generator_zip, generator_stickers_box]
    inlines = [RangeBoxInline]

    def file_link(self, obj):
        if obj.file:
            return "<a href='%s' download>Download</a>" % (obj.file_to_upload.url,)
        else:
            return "No attachment"
    file_link.allow_tags = True
    file_link.short_description = 'File Download'

admin.site.register(Lists, ListsAdmin)



