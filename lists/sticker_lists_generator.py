from docx import Document
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt
from docx.shared import Cm
from docx.enum.section import WD_ORIENT

def sticker_lists_generator(lists_to_sticker_up, lists_to_sticker_down, path_file):
    sticker_up = Document()
    sec = sticker_up.sections[0]
    sec.page_height = Cm(7.5)
    sec.page_width = Cm(15)
    sec.orientation = WD_ORIENT.LANDSCAPE
    sec.left_margin = Cm(0.8)
    sec.right_margin = Cm(0.8)
    sec.top_margin = Cm(0.8)
    sec.bottom_margin = Cm(0.8)
    for jupi in lists_to_sticker_up:
        p = sticker_up.add_paragraph()
        for g in jupi:
            p = sticker_up.add_paragraph()
            p.alignment = WD_ALIGN_PARAGRAPH.CENTER
            font_conf = p.add_run(g)
            font_conf.font.name = 'Calibri'
            font_conf.font.bold = True
            font_conf.font.size = Pt(13)
        sticker_up.add_page_break()

    nazwa_pliku_docx = path_file / 'etykieta_gora.docx'
    sticker_up.save(nazwa_pliku_docx)

    sticker_down = Document()
    sec = sticker_down.sections[0]
    sec.page_height = Cm(7.5)
    sec.page_width = Cm(15)
    sec.orientation = WD_ORIENT.LANDSCAPE
    sec.left_margin = Cm(0.8)
    sec.right_margin = Cm(0.8)
    sec.top_margin = Cm(0.8)
    sec.bottom_margin = Cm(0.8)
    i=1
    for jupi_dol in lists_to_sticker_down:
        p = sticker_down.add_paragraph()
        for g in jupi_dol:
            p = sticker_down.add_paragraph()
            if i != 3:
                p.alignment = WD_ALIGN_PARAGRAPH.CENTER
            else:
                p.alignment = WD_ALIGN_PARAGRAPH.LEFT
            font_conf = p.add_run(g)
            font_conf.font.name = 'Calibri'
            font_conf.font.bold = True
            font_conf.font.size = Pt(10)
            i = i + 1
        sticker_down.add_page_break()
        i = 1

    nazwa_pliku_docx = path_file / 'etykieta dol.docx'
    sticker_down.save(nazwa_pliku_docx)

def sticker_box_generator(lista_to_box, path_file):
    sticker_box = Document()
    sec = sticker_box.sections[0]
    sec.page_height = Cm(7.5)
    sec.page_width = Cm(15)
    sec.left_margin = Cm(0.8)
    sec.right_margin = Cm(0.8)
    sec.top_margin = Cm(0.8)
    sec.bottom_margin = Cm(0.8)
    i=1
    for abc in lista_to_box:
        for g in abc:
            p = sticker_box.add_paragraph()
            p.alignment = WD_ALIGN_PARAGRAPH.CENTER
            font_conf = p.add_run(g)
            font_conf.font.name = 'Calibri'
            font_conf.font.bold = True
            font_conf.font.size = Pt(13)
        #sticker_box.add_page_break()
    nazwa_pliku_docx = path_file / 'etykieta pudelka.docx'
    sticker_box.save(nazwa_pliku_docx)

