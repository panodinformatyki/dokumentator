from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from client.models import Klient

# Create your models here


class TypesSticker(models.Model):
    rodzaj = models.CharField(max_length=15, null=True, blank=True)

    def __str__(self):
        return self.rodzaj


class StickerUpper(models.Model):
    klient = models.OneToOneField(Klient, on_delete=models.CASCADE, primary_key=True)
    types_stickers = models.ForeignKey(TypesSticker, on_delete=models.CASCADE)


class StickerField(models.Model):
    types_sticker = models.ForeignKey(TypesSticker, on_delete=models.CASCADE)
    # opcja1_id = models.PositiveIntegerField()
    # opcja1 = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    # # field_2 = models.ForeignKey(ContentType, on_delete=models.PROTECT
    # # field_2_id = models.PositiveIntegerField()
    # # field_3 = models.ForeignKey(ContentType, on_delete=models.PROTECT
    # # field_3_id = models.PositiveIntegerField()
    # content_object = GenericForeignKey(()
    # field2 = models.ForeignKey(Field1)

# class StickerBriefCases(models.Model):
#     klient = models.ForeignKey(Klient, on_delete=models.CASCADE)
#     one_position=
#     second_position=
#     third_position=