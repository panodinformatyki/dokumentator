from django.apps import AppConfig


class BriefcasesConfig(AppConfig):
    name = 'briefcases'
    verbose_name = 'Baza danych'
