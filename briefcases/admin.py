from django.contrib import admin

#from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib.contenttypes.admin import GenericTabularInline

from .models import BriefCase, Projekty, Jrwa
# Register your models here.

class BriefCaseInline(admin.TabularInline):
    model = BriefCase
    ordering = ("projekty__numer_wniosku_do_segregowania", "jrwa", "data_rozpoczecia", 'data_zakonczenia',)


@admin.register(Projekty)


class ProjektyAdmin(admin.ModelAdmin):
    inlines = [BriefCaseInline]


class JrwaAdmin(admin.ModelAdmin):
    list_display = ('znak_jrwa', 'haslo_jrwa', 'klient')
    list_filter = ('klient',)

admin.site.register(Jrwa, JrwaAdmin)

@admin.register(BriefCase)

class BriefCaseAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)
    list_display = ['numer_spisu', 'projekty', 'jrwa', 'data_rozpoczecia', 'data_zakonczenia', 'user']
    list_filter = ('numer_spisu', 'projekty')

