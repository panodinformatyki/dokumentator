from django.shortcuts import render
from django.http import HttpResponse
from .models import BriefCase

def home(request):
    briefcases = BriefCase.objects.all()
    return render(request, 'naklejka.html', {'briefcases': briefcases})
#     briefcases = BriefCase.objects.all()
#     briefcase_names = list()
#
#     for briefcase in briefcases:
#         briefcase_names.append(briefcase.beneficjent)
#
#     response_html = '<br>'.join(briefcase_names)
#
#     return HttpResponse(response_html)
# # Create your views here.

