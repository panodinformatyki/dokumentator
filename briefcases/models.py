from django.db import models
from django.contrib.auth.models import User
from client.models import Klient

from lists.models import Lists

# Create your models here.

class Projekty(models.Model):
    beneficjent = models.CharField(max_length=255, default='', blank=True)
    numer_wniosku = models.CharField(max_length=255, default='', blank=True)
    opis_projektu = models.CharField(max_length=255, default='', blank=True)
    numer_wniosku_do_segregowania = models.IntegerField(default=0, blank=True, null=True, verbose_name='numer wniosku do segregowania')

    def __str__(self):
        if self.beneficjent == '' or self.opis_projektu == '':
            return self.opis_projektu + self.beneficjent
        else:
            return self.opis_projektu + ' - ' + self.beneficjent

    class Meta:
        ordering = ['numer_wniosku_do_segregowania']
        verbose_name = 'Projekt'
        verbose_name_plural = "Projekty"

class Jrwa(models.Model):
    znak_jrwa = models.CharField(max_length=10, null=True)
    haslo_jrwa = models.CharField(max_length=255, null=True)
    klient = models.ForeignKey(Klient, on_delete=models.CASCADE, null=True)


    def __str__(self):
        return self.znak_jrwa
    class Meta:
        ordering = ['znak_jrwa']
        verbose_name = 'JRWA'
        verbose_name_plural = "JRWA"

class BriefCase(models.Model):
    data_rozpoczecia = models.IntegerField(null=True)
    data_zakonczenia = models.IntegerField(null=True)
    data_zalozenia = models.IntegerField(blank=True, null=True)
    projekty = models.ForeignKey(Projekty, on_delete=models.CASCADE, null=True, blank=True)
    jrwa = models.ForeignKey(Jrwa, on_delete=models.CASCADE, null=True)
    kategoria_archiwalna = models.CharField(max_length=5, null=True)
    numer_spisu = models.ForeignKey(Lists, on_delete=models.CASCADE, null=True)
    tom = models.IntegerField(default=1, null=True)
    zalaczniki = models.CharField(max_length=100, blank=True)
    uwagi = models.CharField(max_length=255, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='entries', verbose_name='Użytkownik')


    def __str__(self):
        return str(self.projekty)

    class Meta:
        ordering = ['data_rozpoczecia']
        verbose_name = 'Teczka'
        verbose_name_plural = "Teczki"


