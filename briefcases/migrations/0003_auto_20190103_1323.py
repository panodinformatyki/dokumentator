# Generated by Django 2.1.4 on 2019-01-03 12:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('briefcases', '0002_auto_20190103_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='projekty',
            name='opis_projektu',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='briefcase',
            name='projekty',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='briefcases.Projekty'),
        ),
    ]
