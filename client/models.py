from django.db import models
#from django import forms

# Create your models here!

MY_CHOICES = (('projekty__beneficjent', 'Beneficjent'),
              ('jrwa__haslo_jrwa', 'Hasło JRWA'),
              ('uwagi', 'Uwagi'),
              ('zalaczniki', 'Załączniki'),
              ('projekty__numer_wniosku', 'Numer Wniosku'))

class Klient(models.Model):
    class Meta():
        verbose_name = "Klient"
        verbose_name_plural = "Klienci"
    nazwa = models.CharField(max_length=255)
    departament = models.CharField(max_length=255)
    skrot_departamentu = models.CharField(max_length=255)
    field_1 = models.CharField(max_length=50, null=True, blank=True, default='0', choices=MY_CHOICES, verbose_name='Pole 1')
    field_2 = models.CharField(max_length=50, null=True, blank=True, default='0', choices=MY_CHOICES, verbose_name='Pole 2')
    field_3 = models.CharField(max_length=50, null=True, blank=True, default='0', choices=MY_CHOICES, verbose_name='Pole 3')
    field_4 = models.CharField(max_length=50, null=True, blank=True, default='0', choices=MY_CHOICES, verbose_name='Pole 4')
    field_5 = models.CharField(max_length=50, null=True, blank=True, default='0', choices=MY_CHOICES, verbose_name='Pole 5')
    #field_5 = forms.ModelChoiceField(max_length=50, null=True, blank=True, default='', choices=MY_CHOICES, verbose_name='Pole 5')
    #adress = models.CharField(max_length=255, n)
    # stawki = models.Charfiel....

    def __str__(self):
        return self.nazwa
