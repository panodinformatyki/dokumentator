from django.contrib import admin

from .models import Klient

class KlientAdmin(admin.ModelAdmin):
    list_display = ('nazwa', 'departament', 'skrot_departamentu')

admin.site.register(Klient, KlientAdmin)

# Register your models here.
